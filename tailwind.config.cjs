/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}'
  ],
  theme: {
    extend: {
      colors: {
        soafee: {
          yellow: '#DEDC00',
          navy: '#03343D',
          black: '#292728',
          'mid-gray': '#C8CBCA',
          gray: '#DCE0E0',
          'light-gray': '#F2F2F2',
          'mid-navy': '#3C6067',
          'dark-gray': '#A6A7A8'
        }
      },
      fontFamily: {
        aspekta: ['var(--font-aspekta)'],
        mono: ['var(--font-roboto-mono)']
      }
    },
    fontFamily: {
      roboto: ['Roboto', 'sans-serif']
    }
  },
  plugins: []
};
