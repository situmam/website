import { globSync } from 'glob';
import matter from 'gray-matter';
import fs from 'node:fs';
import * as path from 'path';

const checkMemberLogos = async () => {
  // get png or jpg files in public/logos
  const memberFiles = globSync('public/logos/*.{png,jpg,jpeg}');

  const memberLogos = globSync('pages/about/members/*.mdx').map((file) => {
    let logo = matter(fs.readFileSync(file)).data.member.logo;
    return logo.split('/')[1];
  });

  const memberLogosWithoutFile = memberFiles.filter((logo) => {
    const logoPath = `${path.basename(logo)}`;
    return !memberLogos.includes(logoPath);
  });

  if (memberLogosWithoutFile.length > 0) {
    console.error('Member logo file (image) exist without a corresponding member file (mdx):', memberLogosWithoutFile);
  }
};

export default checkMemberLogos;
