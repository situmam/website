import { globSync } from 'glob';
import matter from 'gray-matter';
import fs from 'node:fs';
import * as path from 'path';

let imports = `import { BlogPost } from 'data/blog-posts';
`;

let exports = `
export default BlogData;
`;

const rootDir = 'blog';
const availableFolders = ['2022', '2023', '2024']; // Add more folders here if you want to add more years

let generateBlogPosts = async (outputDir) => {
  // Ensure the output directory exists
  if (!fs.existsSync(outputDir)) {
    fs.mkdirSync(outputDir);
  }

  // Find all .mdx pages in the blog directory
  let fm = availableFolders.flatMap((folder) => {
    return (
      globSync(`pages/${rootDir}/${folder}/*.mdx`)
        // Extract frontmatter
        .map((file) => {
          let post = matter(fs.readFileSync(file)).data;
          // Extract id from filename, this is guaranteed to be unique as they are all in the same directory
          post.id = path.basename(file, '.mdx');
          post.folder = `/${rootDir}/${folder}`;
          return post;
        })
    );
  });

  // Generate data
  let items = fm
    .sort((a, b) => new Date(b.date) - new Date(a.date)) // Sort by date
    .map((d) => {
      return `{
    date: '${d.date}',
    title: '${d.title}',
    body: '${d.body}',
    learnMoreLink: '${d.folder}/${d.id}'
  }`;
    });

  let out = fs.createWriteStream(path.resolve(outputDir, 'blog-posts.ts'));

  // Add boilerplate imports
  out.write(imports);
  // Add the events array
  out.write('\nconst BlogData: BlogPost[] = [\n');
  items.map((line, i) => out.write(`  ${line}${i === items.length - 1 ? '' : ','}\n`));
  out.write('];\n');
  // Add the exports definition
  out.write(exports);
};

export default generateBlogPosts;
