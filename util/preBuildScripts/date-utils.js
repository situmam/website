export const isWithinNMonthsOfToday = (dateString, monthCount) => {
  const now = new Date();
  // set date `monthCount` months before today
  const sixMonthBeforeNow = new Date(now).setMonth(now.getMonth() - monthCount);
  const date = new Date(dateString);
  return date > sixMonthBeforeNow;
};
