import { StaticImageData } from 'next/image';
import EventsData from 'autogen/events';

type Event = {
  date: string;
  title: string;
  mobileTitle?: string;
  body: string;
  isHero: boolean;
  heroBody: string;
  heroImage?: StaticImageData | string;
  learnMoreLink: string;
  image: StaticImageData;
};

const Events: Event[] = EventsData;

export { type Event, Events };
