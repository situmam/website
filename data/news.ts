import { StaticImageData } from 'next/image';
import NewsData from 'autogen/news';

type NewsItem = {
  date: string;
  title: string;
  mobileTitle?: string;
  body: string;
  isHero: boolean;
  heroBody: string;
  heroImage?: StaticImageData | string;
  learnMoreLink: string;
  image: StaticImageData | string;
};

const News: NewsItem[] = NewsData;

export { type NewsItem, News };
