import BlogPostsData from 'autogen/blog-posts';

type BlogPost = {
  date: string;
  title: string;
  body: string;
  learnMoreLink: string;
};

const BlogPosts: BlogPost[] = BlogPostsData;

export { type BlogPost, BlogPosts };
