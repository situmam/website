export interface Seminarlink {
  title: string;
  link: string;
}

export const seminarMaterialsData: Seminarlink[] = [
  {
    title: 'Arm-SOAFEE Technical Steering Committee Updates',
    link: '/files/china_seminar/2_arm-soafee_technical_steering_committee_updates.pdf'
  },
  {
    title: 'AWS-Build a SOAFEE-based, cloud-native peer-to-peer development environment via Amazon Web Services',
    link: '/files/china_seminar/3_aws-build_soafee-based_cloud-native_peer-to-peer_development_environment_via_amazon_web_services.pdf'
  },
  {
    title: 'Volcano Engine-Cloud-Vehicle Computer Platform ——Open a new experience in the intelligent cockpit',
    link: '/files/china_seminar/5_volcano_engine-cloud-vehicle_computer_platform_——open_a_new_experience_in_the_intelligent_cockpit.pdf'
  },
  {
    title: 'Denso-SDV Challenges and Cloud-native System Design Approach',
    link: '/files/china_seminar/7_denso-sdv_challenges_and_cloud-native_system_design_approach.pdf'
  },
  {
    title: 'Panasonic-Enabling a Software-Defined Automotive Edge with VirtIO Based Device Virtualization',
    link: '/files/china_seminar/8_panasonic-enabling_a_software-defined_automotive_edge_with_virtio_based_device_virtualization.pdf'
  },
  {
    title: 'Hitachi Astemo-Internet of Vehicles Platform-Towards Value-First Development',
    link: '/files/china_seminar/9_hitachi_astemo-internet_of_vehicles_platform-towards_value-first_development.pdf'
  },
  {
    title: 'LG Electronics-Progress of PICCOLO and Vision of SDV through PICCOLO',
    link: '/files/china_seminar/10_lg_electronics-progress_of_piccolo_and_vision_of_sdv_through_piccolo.pdf'
  },
  {
    title: 'VicOne-Roadmap to Resilience-SDV cybersecurity challenges and opportunities',
    link: '/files/china_seminar/11_vicone-roadmap_to_resilience-sdv_cybersecurity_challenges_and_opportunities.pdf'
  },
  {
    title: 'Black Sesame-Investigating Foundational Technologies for Cross-Domain Computing in Intelligent Vehicles',
    link: '/files/china_seminar/13_black_sesame-investigating_foundational_technologies_for_cross-domain_computing_in_intelligent_vehicles.pdf'
  },
  {
    title: 'OpenSDV-Vehicle Native Operating System',
    link: '/files/china_seminar/14_opensdv-vehicle_native_operating_system.pdf'
  }
];
