import { IconType } from '@/components/icon';
import { IconTextLinkItemProps } from '@/components/icon_text_link_item';

export const JoinItemsGroups: IconTextLinkItemProps[] = [
  {
    iconType: IconType.UserGroup,
    text: 'Governing Body',
    url: 'https://groups.google.com/a/soafee.io/g/gb'
  },
  {
    iconType: IconType.Cog2,
    text: 'Technical Steering Committee',
    url: 'https://groups.google.com/a/soafee.io/g/tsc'
  },
  {
    iconType: IconType.Click,
    text: 'Marketing Steering Committee',
    url: 'https://groups.google.com/a/soafee.io/g/msc'
  }
];

export const JoinItemsSharedDrives: IconTextLinkItemProps[] = [
  {
    iconType: IconType.UserGroup,
    text: 'Governing Body',
    url: 'https://drive.google.com/drive/folders/0AGqsh4Kngv8NUk9PVA'
  },
  {
    iconType: IconType.Cog2,
    text: 'Technical Steering Committee',
    url: 'https://drive.google.com/drive/folders/0AKB1Up6qx-xRUk9PVA'
  },
  {
    iconType: IconType.Click,
    text: 'Marketing Steering Committee',
    url: 'https://drive.google.com/drive/folders/0AK0EE-tDa7f9Uk9PVA'
  }
];
