export interface BluePrint {
  title: string;
  body: string;
  releaseDate: string;
  link?: string;
}

export const blueprints: BluePrint[] = [
  {
    title: 'Autoware Open AD Kit',
    body: `Open AD Kit is developed by the Autoware Foundation (AWF) and work is done in the Open AD Kit Work Group. The project aims to bring software defined best practices to the open-source autonomous drive software stack Autoware, e.g., by introducing cloud-native development and production methodologies.`,
    releaseDate: '10-2022',
    link: 'https://gitlab.com/soafee/blueprints/open-ad-kit'
  }
];
