import Link from 'next/link';
import Border from './border';
import Image, { StaticImageData } from 'next/image';
import Icon, { IconType } from './icon';
import { useEffect, useRef, useState } from 'react';
import { formatDate } from 'util/date';

export interface ICardWithPagination {
  date?: string;
  title?: string;
  body: string;
  learnMoreLink: string;
  link?: string;
  image?: StaticImageData;
}

interface CardsWithPaginationProps {
  backgroundClass: string;
  cards: ICardWithPagination[];
  paginationMethod?: 'view-more' | 'numbered-pages';
  ctaText?: string;
  externalLinks?: boolean;
}

const VIEW_MORE_COUNT = 3;
const PAGE_COUNT = 12;

export default function CardsWithPagination({
  backgroundClass,
  cards,
  paginationMethod,
  ctaText,
  externalLinks
}: CardsWithPaginationProps) {
  const numberOfCardsPerPage = !paginationMethod
    ? cards.length
    : paginationMethod === 'view-more'
    ? VIEW_MORE_COUNT
    : PAGE_COUNT;
  const [userLocale, setUserLocale] = useState('en');
  const [filteredCards, setFilteredCards] = useState<ICardWithPagination[]>(cards.slice(0, numberOfCardsPerPage));
  const [currentPage, setCurrentPage] = useState(1);
  const [initialLoad, setInitialLoad] = useState(true);
  const totalOfPages = Math.ceil(cards.length / PAGE_COUNT);
  const cardsContainerRef = useRef<HTMLDivElement>(null);
  const viewMoreRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    // Obtener el idioma del navegador del usuario después de que el componente se haya montado
    setUserLocale(navigator.language);
  }, []);

  useEffect(() => {
    if (!initialLoad && cardsContainerRef.current) {
      cardsContainerRef.current.scrollIntoView({ behavior: 'smooth' });
    } else {
      setInitialLoad(false);
    }
  }, [currentPage]);

  const handleViewMore = () => {
    viewMoreRef.current && viewMoreRef.current.scrollIntoView({ behavior: 'smooth' });
    setFilteredCards(cards.slice(0, filteredCards.length + numberOfCardsPerPage));
  };

  const handlePreviousPage = () => {
    setFilteredCards(cards.slice(numberOfCardsPerPage * (currentPage - 2), numberOfCardsPerPage * (currentPage - 1)));
    setCurrentPage((prev) => prev - 1);
  };
  const handleNextPage = () => {
    setFilteredCards(
      cards.slice(numberOfCardsPerPage * currentPage, numberOfCardsPerPage * currentPage + numberOfCardsPerPage)
    );
    setCurrentPage((prev) => prev + 1);
  };

  const getTwoDigitsNumber = (number: number) => (number.toString().length === 1 ? '0' + number.toString() : number);

  return (
    <div className={`wrapper-lg pt-20 lg:pt-[86px] lg:pb-[100px]  ${backgroundClass}`} ref={cardsContainerRef}>
      <div
        className={`cards-with-pagination grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 md:gap-x-8 lg:gap-x-0 gap-y-20 lg:gap-y-28`}
      >
        {filteredCards.map((card, index) => {
          const cardLink = card.learnMoreLink || card.link;
          return (
            <div key={index} className="item relative flex flex-col lg:flex-row min-h-[310px] w-full lg:max-w-[500px] ">
              <div className="flex flex-col justify-between lg:pr-20 mb-20 md:mb-0 h-full">
                <div className="mb-9 lg:mb-0">
                  {card.date ? (
                    <div className="typography-tag1 mb-7 lg:mb-5 text-soafee-navy">
                      {formatDate(card.date, userLocale)}
                    </div>
                  ) : null}
                  {card.image && (
                    <div className="mb-6 lg:mb-5">
                      <Image src={card.image} alt={card.title || ''} className="max-h-[74px] w-auto" />
                    </div>
                  )}
                  {card.title ? (
                    <div className="typography-mobile-h4 lg:typography-desktop-b1 mb-6 lg:mb-5 text-soafee-navy">
                      {card.title}
                    </div>
                  ) : null}
                  <div className="typography-mobile-tag3 lg:typography-desktop-b4">{card.body}</div>
                </div>

                {cardLink && (
                  <div className="z-50">
                    <Link href={cardLink} target={externalLinks ? '_blank' : '_self'}>
                      <Border className={backgroundClass}>{ctaText ?? 'Learn More'}</Border>
                    </Link>
                  </div>
                )}
              </div>
            </div>
          );
        })}
      </div>
      {paginationMethod && filteredCards.length < cards.length ? (
        <div className="flex justify-center items-center gap-x-3 text-soafee-navy mt-16 lg:mt-20">
          {paginationMethod === 'view-more' ? (
            <div ref={viewMoreRef}>
              <button className="typography-tag3 flex justify-center items-center gap-x-3" onClick={handleViewMore}>
                View More <Icon className="w-5" type={IconType.ArrowDown} />
              </button>
            </div>
          ) : null}
          {paginationMethod === 'numbered-pages' ? (
            <div className="flex flex-col items-center">
              <div className="flex justify-center items-center gap-x-10">
                <button
                  className="typography-tag3 disabled:cursor-not-allowed"
                  onClick={handlePreviousPage}
                  disabled={currentPage === 1}
                >
                  <Icon className="w-10" type={IconType.ArrowLeft} />
                </button>
                <button
                  className="typography-tag3 disabled:cursor-not-allowed"
                  onClick={handleNextPage}
                  disabled={currentPage === totalOfPages}
                >
                  <Icon className="w-10" type={IconType.ArrowRight} />
                </button>
              </div>
              <div className="typography-tag1 text-soafee-navy mt-2">
                {`${getTwoDigitsNumber(currentPage)}/${getTwoDigitsNumber(totalOfPages)}`}
              </div>
            </div>
          ) : null}
        </div>
      ) : null}
    </div>
  );
}
