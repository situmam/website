import Decorator, { DecorationType } from './decorator';

interface DecoratedTextProps {
  children: React.ReactNode;
  decoration?: DecorationType;
  className?: string;
  decorationClass?: string;
}

export default function DecoratedText({ className, children, decoration, decorationClass }: DecoratedTextProps) {
  return (
    <div className={`relative ${className}`}>
      {children}
      <Decorator decoration={decoration} decorationClass={decorationClass}></Decorator>
    </div>
  );
}
