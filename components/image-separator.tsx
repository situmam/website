import { StaticImageData } from 'next/image';
import React from 'react';
type Props = {
  desktopBackground: StaticImageData;
  mobileBackground: StaticImageData;
};

const ImageSeparator = ({ desktopBackground, mobileBackground }: Props) => {
  return (
    <>
      <div className="hidden md:block relative">
        <svg className="absolute right-0 -bottom-0 fill-[#071231]" height={50} width={100}>
          <polygon points="0, 50 100, 50, 100,0" />
        </svg>
      </div>
      <div
        className="block lg:hidden !bg-no-repeat !bg-cover !bg-bottom w-full h-[400px]"
        style={{ background: `url(${mobileBackground.src})` }}
      />
      <div
        className="hidden lg:block !bg-no-repeat !bg-cover !bg-bottom w-full h-[500px]"
        style={{ background: `url(${desktopBackground.src})` }}
      />
      <div className="hidden md:block relative">
        <svg className="absolute right-0 -bottom-0 fill-soafee-light-gray" height={50} width={100}>
          <polygon points="0, 50 100, 50, 100,0" />
        </svg>
      </div>
    </>
  );
};
export default ImageSeparator;
