import { ReactNode } from 'react';
import HorizontalScrollList from './horizontal_scroll_list';
import { StaticImageData } from 'next/image';
import { Icon, IconType } from './icon';

type PanelTextProps = {
  children: ReactNode;
  className?: string;
};

const PanelText = ({ children, className }: PanelTextProps) => {
  return (
    <div className={`pt-16 w-5/6 ${className}`}>
      <div className="panel-children font-aspekta">{children}</div>
    </div>
  );
};

type PanelLinksProps = {
  children: ReactNode;
  className?: string;
};

const PanelLinks = ({ children, className }: PanelLinksProps) => {
  return (
    <div className={`mt-20 pb-12 ${className}`}>
      <div className="md:hidden flex h-3 mb-6 gap-y-2">
        <Icon className="w-10" type={IconType.ArrowLeft} />
        <Icon className="w-10" type={IconType.ArrowRight} />
      </div>
      <HorizontalScrollList className="no-scrollbar flex">{children}</HorizontalScrollList>
    </div>
  );
};

type PanelLinkItemProps = {
  children: ReactNode;
  icon_type: IconType;
  index: number;
};

const PanelLinkItem = (props: PanelLinkItemProps) => {
  return (
    <div className="flex-shrink-0 mb-2 px-6 border-l-2 border-black w-[250px] h-[230px]" key={props.index}>
      <Icon className="w-[32px] h-[32px] mb-4 lg:mb-3" type={props.icon_type} />
      {props.children}
    </div>
  );
};

type PanelProps = {
  children: ReactNode;
  color: string;
  heroImage: StaticImageData;
  className?: string;
};

const Panel = (props: PanelProps) => {
  return (
    <>
      <div className={`pl-[4%] ${props.className || ''}`} style={{ backgroundColor: props.color }}>
        {props.children}
      </div>
    </>
  );
};

export { Panel, PanelText, PanelLinks, PanelLinkItem };
export default Panel;
