import { CSSProperties, ReactNode } from 'react';
import Image from 'next/image';
import DecoratedImage from './decorated_image';

type TwoLongProps = {
  children: ReactNode;
  style: CSSProperties;
};

const TwoLong = (props: TwoLongProps) => {
  return <div style={{ paddingLeft: '20px', paddingRight: '0', ...props.style }}>{props.children}</div>;
};

type TwoLongTextProps = {
  children: ReactNode;
};

const TwoLongText = (props: TwoLongTextProps) => {
  return <>{props.children}</>;
};

type TwoLongImageProps = {
  image: any;
  alt: string;
  decoration?: 'left' | 'right' | 'none';
  decorationClass?: string;
  showSeparator?: boolean;
};

const TwoLongImage = (props: TwoLongImageProps) => {
  return (
    <div className={`relative pr-0 lg:pr-10`}>
      <DecoratedImage
        src={props.image}
        alt={props.alt}
        decoration={props.decoration}
        decorationClass={props.decorationClass}
        showSeparator={props.showSeparator}
      />
    </div>
  );
};

export { TwoLong, TwoLongImage, TwoLongText };

export default TwoLong;
