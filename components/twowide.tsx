import { CSSProperties, ReactNode } from 'react';
import Image from 'next/image';
import DecoratedImage from './decorated_image';
import ReactMarkdown from 'react-markdown';

// TODO: Check children of TwoWide and restrict to allowd values

type Props = {
  children: ReactNode;
  right: boolean;
  image: any | null;
  title: string | null;
  triangle: string | null;
  style: CSSProperties | null;
};

type TwoWideProps = {
  children: ReactNode;
  style: CSSProperties | null;
  className: string;
};
// Bootstrap adds the lines
//   margin-right: calc(-0.5 * var(--bs-gutter-x));
//   margin-left: calc(-0.5 * var(--bs-gutter-x));
// which causes the horizontal sizing of the render to overflow the screen
// which adds a scroll bar at the bottom.  This may be a bug in bootstrap
// or an error in the way it is being used here, but this is why the lines
//
const TwoWide = (props: TwoWideProps) => {
  let style: CSSProperties = {
    marginLeft: '0!important',
    marginRight: '0!important'
  };

  return (
    <div className={props.className} style={{ ...style, ...props.style } || style}>
      {props.children}
    </div>
  );
};

type TwoWideTextProps = {
  children: ReactNode;
};

const TwoWideText = (props: TwoWideTextProps) => {
  return <div className={'twowide-text align-self-center px-5 py-5'}>{props.children}</div>;
};

type TwoWideImageProps = {
  right: boolean;
  image: any | null;
  triangle: 'left' | 'right' | 'none';
};

const TwoWideImage = (props: TwoWideImageProps) => {
  return (
    <div className="twowide-image px-0" style={{ position: 'relative', fill: 'white' }}>
      <DecoratedImage src={props.image} alt="..." decoration={props.triangle} />
    </div>
  );
};

type TwoWideTitleProps = {
  children: ReactNode;
};

const TwoWideTitle = (props: TwoWideTitleProps) => {
  return <div className="align-self-center twowide-title px-5 pt-5">{props.children}</div>;
};

//export default TwoWide;
export { TwoWide, TwoWideText, TwoWideImage, TwoWideTitle };
