import { Members, Member } from 'data/members';
import Image from 'next/image';

type Props = {
  member?: Member;
  memberId?: Member | string;
  alt?: string;
  useInternal?: boolean;
  noLink?: boolean;
};

const getMember = (member?: Member | string) => {
  if (typeof member === 'string') {
    return Members.find((item: Member) => item.id == member);
  } else {
    // The website will be unique, search for the member by this id
    return Members.find((item: Member) => item.website == member?.website);
  }
};

const MemberLogo = ({ member, memberId, alt }: Props) => {
  let currentMember: Member | undefined | null = member || null;
  if (!currentMember) {
    if (!memberId) return <></>;
    currentMember = getMember(memberId);
    if (currentMember == undefined) {
      console.error('Member not found %s', memberId);
      return <></>;
    }
  }

  return (
    <Image
      className="aspect-[16/9] object-contain w-40 xl:w-52"
      src={currentMember.imageData.src}
      alt={alt || currentMember.name}
      width={208}
      height={117}
    />
  );
};

export default MemberLogo;
