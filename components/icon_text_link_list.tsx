import { IconTextLinkItem, IconTextLinkItemProps } from './icon_text_link_item';
import Icon, { IconType } from './icon';

export interface IconTextLinkListProps {
  className?: string;
  borderClass?: string;
  items?: IconTextLinkItemProps[];
}

export function IconTextLinkList({ className = '', items = [], borderClass }: IconTextLinkListProps) {
  return (
    <div className="mt-16 lg:mt-28">
      <div className="md:hidden flex h-5 mb-5 gap-2 px-0">
        <Icon className="" type={IconType.ArrowLeft} />
        <Icon className="" type={IconType.ArrowRight} />
      </div>
      <div
        className={`${className} flex overflow-y-hidden overflow-x-auto  md:grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 h-fit`}
      >
        {items.map((item, index) => {
          return (
            <IconTextLinkItem
              key={index}
              className="mr-16 lg:mr-5 mb-20"
              url={item.url}
              borderClass={borderClass}
              iconType={item.iconType}
              text={item.text}
            />
          );
        })}
      </div>
    </div>
  );
}
