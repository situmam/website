import Image from 'next/image';
import { Member, Members, MemberType } from 'data/members';
import { Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';

const members = Members.filter((member: Member) => member.type == MemberType.GoverningBody).sort(
  (a: Member, b: Member) => a.name.localeCompare(b.name)
);

const MemberTicker = () => {
  return (
    <div className="flex w-full">
      <Swiper
        modules={[Autoplay]}
        centeredSlides={true}
        slidesPerView={2}
        allowTouchMove={false}
        spaceBetween={50}
        autoplay={{ delay: 2000 }}
        loop={true}
      >
        {members.map((member) => (
          <SwiperSlide key={member.id}>
            <div className="flex justify-center items-center h-full">
              <Image src={member.imageData} alt={member.name} />
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default MemberTicker;
