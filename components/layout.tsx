import Head from 'next/head';
import Footer from './footer';
import Header from './header';
import MemberLogo from './memberlogo';
import CookieConsent from './cookie_consent';
import { Member } from 'data/members';
import Image from 'next/image';

type FrontMatter = {
  layout?: string;
  title?: string;
  description?: string;
  member?: Member;
  memberId?: string;
  logo?: string;
  hideLogo?: boolean;
};

type Props = {
  frontmatter?: FrontMatter;
  fontClassName: string;
  preview?: boolean;
  children: React.ReactNode;
};

const MemberPage = (props?: Props) => {
  let id = props?.frontmatter?.member || props?.frontmatter?.memberId;
  return (
    <>
      <div className="justify-content-center" style={{ width: '30%' }}>
        <MemberLogo memberId={id} />
      </div>
      {props?.children}
    </>
  );
};

const NewsTemplate = (props?: Props) => {
  return (
    <main className="pt-[80px] lg:pt-[102px]">
      {props?.frontmatter?.hideLogo
        ? null
        : !(
            <div className="wrapper-lg">
              <Image src={props?.frontmatter?.logo!} alt={props?.frontmatter?.logo!} width={200} height={200} />
            </div>
          )}
      {props?.children}
    </main>
  );
};

const DefaultPage = (props?: Props) => {
  return props?.children;
};

const Layout = (props?: Props) => {
  let generateContent = (props?: Props) => {
    let layout = props?.frontmatter?.layout;
    if (layout == undefined || layout == 'default') {
      return DefaultPage(props);
    } else if (layout == 'memberPage') {
      return MemberPage(props);
    } else if (layout == 'newsTemplate') {
      return NewsTemplate(props);
    }
    // Force a compilation error, but use @ts-ignore to stop this being
    // a error in your editor
    // @ts-ignore
    error;
  };

  return (
    <>
      <Head>
        <title>{props?.frontmatter?.title}</title>
        <meta name="description" content={props?.frontmatter?.description} />
        <link rel="icon" type="image/svg+xml" href="/favicon.svg" />
      </Head>

      <Header className={`${props?.fontClassName}`} />

      <main className={`${props?.fontClassName}`}>
        <div className="gx-0">{generateContent(props)}</div>
      </main>

      <Footer className={props?.fontClassName} />
      <CookieConsent />
    </>
  );
};

export default Layout;
