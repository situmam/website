import { ReactNode } from 'react';

type Props = {
  children: ReactNode;
  className?: string;
};

const Border = ({ children, className = 'bg-soafee-yellow ' }: Props) => {
  return (
    <div className="relative border-black border-[1.18px] inline-block pl-9 pr-10 py-2 font-[550] text-[16px] font-aspekta text-soafee-black">
      {children}
      <div
        className={`${className} absolute -bottom-[18px] -right-[11px] transform rotate-[55deg] h-7 w-7 border-l-[1.18px] border-black`}
      />
    </div>
  );
};

export default Border;
