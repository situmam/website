import '../styles/main.scss';
import '../styles/index.css';
import type { AppProps } from 'next/app';
import Layout from '@/components/layout';
import GoogleAnalytics from '@/components/google_analytics';
import localFont from 'next/font/local';
import { Roboto_Mono } from 'next/font/google';

const robotoMono = Roboto_Mono({
  subsets: ['latin'],
  variable: '--font-roboto-mono'
});

const aspekta = localFont({
  variable: '--font-aspekta',
  src: [
    {
      path: './fonts/Aspekta-100.otf',
      weight: '100',
      style: 'normal'
    },
    {
      path: './fonts/Aspekta-200.otf',
      weight: '200',
      style: 'normal'
    },
    {
      path: './fonts/Aspekta-350.otf',
      weight: '350',
      style: 'normal'
    },
    {
      path: './fonts/Aspekta-550.otf',
      weight: '550',
      style: 'normal'
    },
    {
      path: './fonts/Aspekta-650.otf',
      weight: '650',
      style: 'normal'
    },
    {
      path: './fonts/Aspekta-750.otf',
      weight: '750',
      style: 'normal'
    },
    {
      path: './fonts/Aspekta-950.otf',
      weight: '950',
      style: 'normal'
    },
    {
      path: './fonts/Aspekta-1000.otf',
      weight: '100',
      style: 'normal'
    }
  ]
});

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <GoogleAnalytics />
      <Layout fontClassName={`${aspekta.variable} ${robotoMono.variable}`} {...pageProps}>
        <Component {...pageProps} />
      </Layout>
    </>
  );
}

export default MyApp;
